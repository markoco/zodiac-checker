<?php
	$fullName = $_POST['fullName'];
	$birthMonth = strtolower($_POST['birthMonth']);
	$birthDay = $_POST['birthDay'];

	$zodiac = "";
		if($birthMonth === "march" && $birthDay >=21){
			$zodiac = "Aries";
		}elseif($birthMonth === "april" && $birthDay <=19){
			$zodiac = "Aries";
		}elseif($birthMonth === "april" && $birthDay >=20){
			$zodiac = "Taurus";
		}elseif($birthMonth === "may" && $birthDay <=20){
			$zodiac = "Taurus";
		}elseif($birthMonth === "may" && $birthDay >=21){
			$zodiac = "Gemeni";
		}elseif($birthMonth === "june" && $birthDay <=20){
			$zodiac = "Gemeni";
		}elseif($birthMonth === "june" && $birthDay >=21){
			$zodiac = "Cancer";
		}elseif($birthMonth === "july" && $birthDay <=22){
			$zodiac = "Cancer";
		}elseif($birthMonth === "july" && $birthDay >=23){
			$zodiac = "Leo";
		}elseif($birthMonth === "august" && $birthDay <=22){
			$zodiac = "Leo";
		}elseif($birthMonth === "august" && $birthDay >=23){
			$zodiac = "Virgo";
		}elseif($birthMonth === "september" && $birthDay <=22){
			$zodiac = "Virgo";
		}elseif($birthMonth === "september" && $birthDay >=23){
			$zodiac = "Libra";
		}elseif($birthMonth === "october" && $birthDay <=22){
			$zodiac = "Libra";
		}elseif($birthMonth === "october" && $birthDay >=23){
			$zodiac = "Scorpio";
		}elseif($birthMonth === "november" && $birthDay <=21){
			$zodiac = "Scorpio";
		}elseif($birthMonth === "november" && $birthDay >=22){
			$zodiac = "Sagittarius";
		}elseif($birthMonth === "december" && $birthDay <=21){
			$zodiac = "Sagittarius";
		}elseif($birthMonth === "december" && $birthDay >=22){
			$zodiac = "Capricorn";
		}elseif($birthMonth === "january" && $birthDay <=19){
			$zodiac = "Capricorn";
		}elseif($birthMonth === "january" && $birthDay >=20){
			$zodiac = "Aquarius";
		}elseif($birthMonth === "february" && $birthDay <=18){
			$zodiac = "Aquarius";
		}elseif($birthMonth === "february" && $birthDay >=19){
			$zodiac = "Pisces";
		}elseif($birthMonth === "march" && $birthDay <=20){
			$zodiac = "Pisces";
		}else{
			$zodiac = "";
		}




	session_start();
	if(strlen($fullName)===0 || strlen($birthMonth)===0 || strlen($birthDay)===0 || $birthDay > 31 || $zodiac == ""){
		header("Location: ". $_SERVER['HTTP_REFERER']); //To go back to registraion-form.php or to where the data came from
		$_SESSION['errorMsg'] = "Please fill up the form properly";
	}else{

		$_SESSION['fullName'] = $fullName; //to access the inputted data from form to any pages
		$_SESSION['zodiac'] = $zodiac;



		header("Location: ../views/landingpage.php"); //sets where to redirect	
	}
	
?>