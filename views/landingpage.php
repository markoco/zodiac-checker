<!DOCTYPE html>
<html>
<head>
	<title>Zodiac Checker</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body class="bg-primary">
	<div class="d-flex align-items-center justify-content-center flex-column vh-100">
		<div class="col-lg-4 d-flex align-items-center justify-content-center flex-column bg-dark">
			<h1 class="text-center text-white">Hello 
				<span class="text-danger">
				<?php
				 	session_start();
				 	echo $_SESSION['fullName'];
			 	?>	
			 	</span>
			 </h1>

			 <h1 class="text-center text-white">
			 	Your Zodiac Sign is
			 	<span class="text-danger">
				 	<?php 
				 	echo $_SESSION['zodiac'];
				 	?>
			 	</span>
			 </h1>
		 </div>
	</div>
</body>
</html>