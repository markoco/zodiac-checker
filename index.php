<!DOCTYPE html>
<html>
<head>
	<title>Zodiac Sign</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body class="bg-primary">
	<h1 class="text-center text-white my-5">Zodiac Sign Checker</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-dark text-white p-4" action="controllers/zodiac-controllers.php" method="POST">
			<div class="form-group">
				<label for="fullName">Full Name</label>
				<input type="text" name="fullName" class="form-control" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month (January-December)</label>
				<input type="text" name="birthMonth" class="form-control" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="birthDay">Birth Day (1-31)</label>
				<input type="number" name="birthDay" class="form-control" autocomplete="off">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check Zodiac</button>
			</div>	


			<?php 
				session_start();
				session_destroy();
				if(isset($_SESSION['errorMsg'])){
			?>	

				<p class="text-danger"><?php echo $_SESSION['errorMsg']; ?></p>
			<?php
				}
			?>
		</form>
	</div>
</body>
</html>